﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringCalculator;

namespace TestCalculator
{
    [TestClass]
    public class CalculatorUnitTest
    {

        [TestMethod]
        public void EmptyString()
        {
            string number = "";
            int expected = 0;
            Calculate calculate = new Calculate();
            var actual = calculate.Add(number);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void EmptyString_And_1_And_2()
        {
            string number = ",1,2";
            int expected = 3;
            Calculate calculate = new Calculate();
            var actual = calculate.Add(number);
            Assert.AreEqual(expected,actual);
        }
        [TestMethod]
        public void Handle_Unknown_Amount_Number()
        {
            string number = "A,D\nE";
            int expected = 0;
            Calculate calculate = new Calculate();
            var actual = calculate.Add(number);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Handle_NewLine_Between_Numbers()
        {
            string number = "1,2\n3";
            int expected = 6;
            Calculate calculate = new Calculate();
            var actual = calculate.Add(number);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Input_End_With_NewLine()
        {
            string number = "1,2\n";
            int expected = 0;
            Calculate calculate = new Calculate();
            var actual = calculate.Add(number);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Support_Different_Single_Delimitors()
        {
            string number = "//*\n1*2";
            int expected = 3;
            Calculate calculate = new Calculate();
            var actual = calculate.Add(number);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Negative_Number_Throw_Exception()
        {
            string number = "//*\n1*-2";
            string message = "-2,Negatives not allowed";
            int expected = 1;
            Calculate calculate = new Calculate();
            var actual = calculate.Add(number);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(message, calculate.Response.Dequeue());
        }
        [TestMethod]
        public void Ignore_Numbers_Greater_Than_1000()
        {
            string number = "2,1001,13";
            int expected = 15;
            Calculate calculate = new Calculate();
            var actual = calculate.Add(number);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Allow_Multiple_Delimitors()
        {
            string number = "//*%\n1*2%3";
            int expected = 6;
            Calculate calculate = new Calculate();
            var actual = calculate.Add(number);
            Assert.AreEqual(expected, actual);
        }
    }
}
 