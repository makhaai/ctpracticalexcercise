﻿using StringCalculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTPracticalExcercise
{
    class Program
    {
        

        static void Main(string[] args)
        {
            Calculate calculate = new Calculate();

            var sum=calculate.Add("1001,2,13,-5,5,-10\n2");

            Console.WriteLine(sum.ToString());

            // display error message
            foreach(string message in calculate.Response)
            {
                Console.WriteLine(message);
            }

            Console.ReadLine();
        }
    }
}
