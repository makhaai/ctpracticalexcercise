﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculator
{
    public class Calculate
    {
        private const int MAX_VALUE = 1000;
        public Queue<string> Response;
        private List<string> delimitors;

        public Calculate()
        {
            Response = new Queue<string>();

            delimitors = new List<string>();
            addDefailtDelimitors();
        }
        public int Add(string numbers)
        {
            var formattedNumber = getNumbersToAdd(numbers);
            return getResults(formattedNumber);
        }


        private void addDefailtDelimitors()
        {
            delimitors.Add(",");
            delimitors.Add("\n");
        }
        private bool isEndWithNewLine(string number)
        {
            return number.EndsWith("\n");
        }
        private void checkIfAnyNewDelimitor(string number) {

            if (number.StartsWith("//"))
            {
                var index = number.IndexOf("\n");
                var newDelimit = number.Substring(2, (index - 2)).ToCharArray();
                foreach (char item in newDelimit)
                {
                    delimitors.Add(item.ToString());
                }
            }
        }
        
        private List<int> getNumbersToAdd(string numbers)
        {
            List<int> output = new List<int>();
            if (!isEndWithNewLine(numbers))
            {
                checkIfAnyNewDelimitor(numbers);

                string[] stringItems = numbers.Split(delimitors.ToArray(), StringSplitOptions.None);
                int index = 0;
                do
                {
                    int isNumber = 0;
                    int.TryParse(stringItems[index], out isNumber); //this will return 0 if item is not integer or empty string

                    isNumber = isNumber > MAX_VALUE ? 0 : isNumber;

                    if (!isNumber.IsNegative())
                    {
                        output.Add(isNumber);
                    }
                    else
                    {
                        string message = string.Format("{0},Negatives not allowed", isNumber.ToString());
                        Response.Enqueue(message);
                    }

                    index++;
                } while (index < stringItems.Length);
            }
            else
            {
                output.Add(0);
            }
            return output;
        }
        private int getResults(List<int> numbers)
        {
            return numbers.Sum();
        }
    }
}
