﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculator
{
    public static class Extend
    {
        public static bool IsNegative(this int number) {

            return number < 0;
        }
    }
}
